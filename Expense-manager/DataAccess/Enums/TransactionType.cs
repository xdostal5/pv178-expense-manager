﻿using Microsoft.CSharp.RuntimeBinder;

namespace DataAccess.Enums;

public enum TransactionType
{
    Income,
    Expense
}