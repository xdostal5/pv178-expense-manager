﻿namespace DataAccess.Enums;

public enum UserRight
{
    Registered,
    Admin,
}