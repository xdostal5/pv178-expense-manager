﻿namespace DataAccess.Configuration;

public class DbConfiguration
{
    public bool UseInMemoryDatabase { get; set; }
    public string ConnectionString { get; set; }
}