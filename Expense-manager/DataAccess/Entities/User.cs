﻿using System.ComponentModel.DataAnnotations;
using DataAccess.Enums;
using Microsoft.EntityFrameworkCore;

namespace DataAccess.Entities;

public class User
{
    [Key]
    public Guid Id { get; set; }
    public string Email { get; set; }
    public string UserName { get; set; }
    public string Password { get; set; }
    public UserRight UserRight { get; set; }
    public bool IsDeleted { get; set; }
}