﻿using System.ComponentModel.DataAnnotations;
using DataAccess.Enums;

namespace DataAccess.Entities;

public class Transaction
{
    [Key]
    public Guid Id { get; set; }
    public string? Name { get; set; }
    public string? Description { get; set; }
    public DateTime Date { get; set; }
    public Category? Category { get; set; }
    public required User User { get; set; }
    public TransactionType TransactionType { get; set; }
    public float amount { get; set; }
}