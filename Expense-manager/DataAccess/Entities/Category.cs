﻿using System.ComponentModel.DataAnnotations;

namespace DataAccess.Entities;

public class Category
{
    [Key]
    public Guid Id { get; set; }
    public string? Name { get; set; }
    public string? Color { get; set; }
    public required User User { get; set; }
}