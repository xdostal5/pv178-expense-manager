﻿using DataAccess.Entities;

namespace DataAccess.Database;

public interface IUserRepository
{
    Task<User?> GetUserByUsername(string userName);

    Task<User?> AddUserAsync(User user);

    Task<User?> UpdateUserAsync(User user);
}