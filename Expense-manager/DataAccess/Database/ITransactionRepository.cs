﻿using DataAccess.Entities;
using DataAccess.Enums;

namespace DataAccess.Database;

public interface ITransactionRepository
{
    Task<List<Transaction>> GetTransactionsFilteredAsync(User user, Category? category,
        TransactionType? transactionType, int take, int skip);

    Task<int> GetTransactionsFilteredCountAsync(User user, Category? category,
        TransactionType? transactionType);

    Task<Transaction?> AddTransactionAsync(Transaction transaction);

    Task<Transaction?> UpdateTransactionAsync(Transaction transaction);

    Task<bool> DeleteTransactionAsync(Transaction transaction);
}