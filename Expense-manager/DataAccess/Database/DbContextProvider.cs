﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using Microsoft.Extensions.Logging;

namespace DataAccess.Database;

public class DbContextProvider : IDbContextProvider
{
    private readonly IDbContextFactory<ApplicationDbContext> _dbContextFactory;
    private readonly ILogger<DbContextProvider> _logger;
    private ApplicationDbContext? _dbContext;
    private IDbContextTransaction? _transaction;
    private bool _isTransaction;

    public DbContextProvider(IDbContextFactory<ApplicationDbContext> dbContextFactory,
        ILogger<DbContextProvider> logger)
    {
        _dbContextFactory = dbContextFactory;
        _isTransaction = false;
        _logger = logger;
    }
    
    
    public async Task<ApplicationDbContext> GetDbContext()
    {
        if (_isTransaction)
        {
            if (_dbContext == null)
            {
                _logger.LogError("Error in GetDbContext: dbContext is null");
                throw new NullReferenceException();
            }

            return _dbContext;
        }

        return await _dbContextFactory.CreateDbContextAsync();
    }

    public async Task BeginTransaction()
    {
        _dbContext = await _dbContextFactory.CreateDbContextAsync();
        _transaction = await _dbContext.Database.BeginTransactionAsync();
        _isTransaction = true;
    }

    public async Task CommitTransaction()
    {
        _isTransaction = false;

        if (_transaction == null)
        {
            _logger.LogError("Error in commit transaction: transaction is null");
            throw new NullReferenceException();
        }

        await _transaction.CommitAsync();
        await _transaction.DisposeAsync();
        _transaction = null;

        if (_dbContext == null)
        {
            _logger.LogError("Error in commit transaction: dbContext is null");
            throw new NullReferenceException();
        }

        await _dbContext.DisposeAsync();
        _dbContext = null;
    }
}