﻿namespace DataAccess.Database;

public interface IDbContextProvider
{
    Task<ApplicationDbContext> GetDbContext();

    Task BeginTransaction();

    Task CommitTransaction();
}