﻿using DataAccess.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace DataAccess.Database;

public class UserRepository : IUserRepository
{
    private readonly IDbContextProvider _dbContextProvider;
    private readonly ILogger<UserRepository> _logger;

    public UserRepository(IDbContextProvider dbContextProvider, ILogger<UserRepository> logger)
    {
        _dbContextProvider = dbContextProvider;
        _logger = logger;
    }
    
    /// <summary>
    /// Tries to retrieve a user by it's name from database
    /// </summary>
    /// <param name="userName"></param>
    /// <returns></returns>

    public async Task<User?> GetUserByUsername(string userName)
    {
        using (var dbContext = await _dbContextProvider.GetDbContext())
        {
            return await dbContext.Users.FirstOrDefaultAsync(u => u.UserName == userName && !u.IsDeleted);
        }
    }
    
    /// <summary>
    /// Tries to add new user if possible to the database
    /// </summary>
    /// <param name="user"></param>
    /// <returns></returns>

    public async Task<User?> AddUserAsync(User user)
    {
        using (var dbContext = await _dbContextProvider.GetDbContext())
        {
            if (await dbContext.Users.FirstOrDefaultAsync(u =>
                    (u.UserName == user.UserName || u.Email == user.Email) && !u.IsDeleted) != null)

            {
                _logger.LogInformation(
                    $"User with username: {user.UserName} or email: {user.Email} already exists");
                return null;
            }

            user.Id = Guid.NewGuid();
            await dbContext.Users.AddAsync(user);
            await dbContext.SaveChangesAsync();

            return user;
        }
    }
    
    /// <summary>
    /// Tries to update an existing user in the database
    /// </summary>
    /// <param name="user"></param>
    /// <returns></returns>

    public async Task<User?> UpdateUserAsync(User user)
    {
        using (var dbContext = await _dbContextProvider.GetDbContext())
        {
            var userToUpdate = await dbContext.Users.FirstOrDefaultAsync(u => u.Id == user.Id && !u.IsDeleted);

            if (userToUpdate == null)
            {
                _logger.LogInformation($"In UpdateUserAsync: user with id: {user.Id} does not exist");
                return null;
            }

            if (await dbContext.Users.FirstOrDefaultAsync(u =>
                    (u.UserName == user.UserName || u.Email == user.Email) && !u.IsDeleted && u.Id != user.Id) != null)
            {
                _logger.LogInformation("In UpdateUserAsync: user desired values are used by other user");
                return null;
            }

            userToUpdate.UserName = user.UserName;
            userToUpdate.Email = user.Email;
            userToUpdate.Password = user.Password;

            await dbContext.SaveChangesAsync();

            userToUpdate = await dbContext.Users.FirstOrDefaultAsync(u => u.Id == user.Id);

            if (userToUpdate == null)
            {
                _logger.LogInformation($"In UpdateUserAsync: user with id: {user.Id} does not exist");
                return null;
            }

            return user;
        }
    }
}