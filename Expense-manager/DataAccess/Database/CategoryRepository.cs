﻿using DataAccess.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace DataAccess.Database;

public class CategoryRepository : ICategoryRepository
{
    private readonly IDbContextProvider _dbContextProvider;
    private readonly ILogger<CategoryRepository> _logger;
    
    public CategoryRepository(IDbContextProvider dbContextProvider, ILogger<CategoryRepository> logger)
    {
        _dbContextProvider = dbContextProvider;
        _logger = logger;
    }
    
    /// <summary>
    /// Tries to retrieve a category by it's id from database
    /// </summary>
    /// <param name="categoryId"></param>
    /// <returns></returns>

    public async Task<Category?> GetCategoryById(Guid categoryId)
    {
        using (var dbContext = await _dbContextProvider.GetDbContext())
        {
            return await dbContext.Categories.FirstOrDefaultAsync(c => c.Id == categoryId);
        }
    }
    
    /// <summary>
    ///  Tries to retrieve all categories corresponding to a user determined by it's ID
    /// </summary>
    /// <param name="userId"></param>
    /// <returns></returns>

    public async Task<List<Category>> GetCategoriesByUserId(Guid userId)
    {
        using (var dbContext = await _dbContextProvider.GetDbContext())
        {
            return await dbContext.Categories.Where(c => c.User.Id == userId).ToListAsync();
        }
    }
    
    /// <summary>
    ///  Tries to add a new category to the database if possible
    /// </summary>
    /// <param name="category"></param>
    /// <returns></returns>

    public async Task<Category?> AddCategoryAsync(Category category)
    {
        using (var dbContext = await _dbContextProvider.GetDbContext())
        {
            if (await dbContext.Categories.FirstOrDefaultAsync(c =>
                    c.Name == category.Name && c.User == category.User) != null)
            {
                _logger.LogInformation(
                    $"Category with name: {category.Name} for user: {category.User.UserName} already exists");
                return null;
            }

            category.Id = Guid.NewGuid();
            await dbContext.Categories.AddAsync(category);
            await dbContext.SaveChangesAsync();

            return category;
        }
    }
    
    /// <summary>
    /// Tries to update a category in the database
    /// </summary>
    /// <param name="category"></param>
    /// <returns></returns>

    public async Task<Category?> UpdateCategoryAsync(Category category)
    {
        using (var dbContext = await _dbContextProvider.GetDbContext())
        {
            var categoryToUpdate = await dbContext.Categories.FirstOrDefaultAsync(c => c.Id == category.Id);

            if (categoryToUpdate == null)
            {
                _logger.LogInformation($"In UpdateCategoryAsync: category with id: {category.Id} does not exist");
                return null;
            }

            categoryToUpdate.Name = category.Name;
            categoryToUpdate.Color = category.Color;

            await dbContext.SaveChangesAsync();

            categoryToUpdate = await dbContext.Categories.FirstOrDefaultAsync(c => c.Id == category.Id);

            if (categoryToUpdate == null)
            {
                _logger.LogInformation($"In UpdateCategoryAsync: category with id: {category.Id} does not exist");
                return null;
            }

            return category;
        }
    }
    
    /// <summary>
    /// Deletes a category in the database and replaces all places where it was referenced to null to keep the DB consistent
    /// </summary>
    /// <param name="category"></param>
    /// <returns></returns>

    public async Task<bool> DeleteCategoryAsync(Category category)
    {
        using (var dbContext = await _dbContextProvider.GetDbContext())
        {
            var categoryToDelete = await dbContext.Categories.FirstOrDefaultAsync(c => c.Id == category.Id);

            if (categoryToDelete == null)
            {
                _logger.LogInformation($"In UpdateCategoryAsync: category with id: {category.Id} does not exist");
                return false;
            }

            var transactionsToCategory = await dbContext.Transactions
                .Where(t => t.Category == category)
                .ToListAsync();

            transactionsToCategory.ForEach(t => t.Category = null);
           
            dbContext.Categories.Remove(categoryToDelete);
            await dbContext.SaveChangesAsync();

            return true;
        }
    }
}