﻿using DataAccess.Entities;
using Microsoft.EntityFrameworkCore;

namespace DataAccess.Database;

/// <summary>
/// base class that holds the dbcontexts for our database.
/// </summary>
public class ApplicationDbContext : DbContext
{
    public DbSet<User> Users { get; set; }
    public DbSet<Transaction> Transactions { get; set; }
    public DbSet<Category> Categories { get; set; }

    public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
    {
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<User>().ToTable("Users");
        modelBuilder.Entity<Transaction>().ToTable("Transactions");
        modelBuilder.Entity<Category>().ToTable("Categories");
        
        base.OnModelCreating(modelBuilder);
    }

    public override void Dispose()
    {
        if (Database.CurrentTransaction == null)
        {
            base.Dispose();
        }
    }
}