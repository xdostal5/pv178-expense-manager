﻿using DataAccess.Entities;
using DataAccess.Enums;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace DataAccess.Database;

public class TransactionRepository : ITransactionRepository
{
    private readonly IDbContextProvider _dbContextProvider;
    private readonly ILogger<TransactionRepository> _logger;
    
    public TransactionRepository(IDbContextProvider dbContextProvider, ILogger<TransactionRepository> logger)
    {
        _dbContextProvider = dbContextProvider;
        _logger = logger;
    }
    
    /// <summary>
    /// Tries to retrieve Transactions from database corresponding to a user. If category and transactionType
    /// are provided, it filters the result according to those arguments. Take, skip arguments can be used
    /// to page the result.
    /// </summary>
    /// <param name="user"></param>
    /// <param name="category"></param>
    /// <param name="transactionType"></param>
    /// <param name="take"></param>
    /// <param name="skip"></param>
    /// <returns></returns>

    public async Task<List<Transaction>> GetTransactionsFilteredAsync(User user, Category? category, 
        TransactionType? transactionType, int take, int skip)
    {
        using (var dbContext = await _dbContextProvider.GetDbContext())
        {
            return await dbContext.Transactions
                .Include(t => t.User)
                .Include(t => t.Category)
                .Where(t => t.User == user && !t.User.IsDeleted)
                .Where(t => category == null || t.Category == category)
                .Where(t => transactionType == null || t.Category == category)
                .Skip(skip)
                .Take(take)
                .ToListAsync();
        }
    }
    
    
    /// <summary>
    /// Return the count of all transactions in the database corresponding to a user. If category and transactionType
    /// are provided, it filters the result according to those arguments. 
    /// </summary>
    /// <param name="user"></param>
    /// <param name="category"></param>
    /// <param name="transactionType"></param>
    /// <returns></returns>
    
    public async Task<int> GetTransactionsFilteredCountAsync(User user, Category? category, 
        TransactionType? transactionType)
    {
        using (var dbContext = await _dbContextProvider.GetDbContext())
        {
            return await dbContext.Transactions
                .Include(t => t.User)
                .Include(t => t.Category)
                .Where(t => t.User == user && !t.User.IsDeleted)
                .Where(t => category == null || t.Category == category)
                .CountAsync(t => transactionType == null || t.Category == category);
        }
    }
    
    /// <summary>
    /// Trie to add a new transaction to the database if possible
    /// </summary>
    /// <param name="transaction"></param>
    /// <returns></returns>

    public async Task<Transaction?> AddTransactionAsync(Transaction transaction)
    {
        using (var dbContext = await _dbContextProvider.GetDbContext())
        {
            if (await dbContext.Transactions.FirstOrDefaultAsync(t => t.Id == transaction.Id) != null)
            {
                _logger.LogInformation($"Transaction with id: {transaction.Id} already exists");
                return null;
            }

            transaction.Id = Guid.NewGuid();
            await dbContext.Transactions.AddAsync(transaction);
            await dbContext.SaveChangesAsync();

            return transaction;
        }
    }
    
    /// <summary>
    /// Tries to update an existing transaction in the database
    /// </summary>
    /// <param name="transaction"></param>
    /// <returns></returns>
    public async Task<Transaction?> UpdateTransactionAsync(Transaction transaction)
    {
        using (var dbContext = await _dbContextProvider.GetDbContext())
        {
            var transactionToUpdate = await dbContext.Transactions.FirstOrDefaultAsync(t => t.Id == transaction.Id);

            if (transactionToUpdate == null)
            {
                _logger.LogInformation($"In UpdateCategoryAsync: category with id: {transaction.Id} does not exist");
                return null;
            }

            transactionToUpdate.Name = transaction.Name;
            transactionToUpdate.TransactionType = transaction.TransactionType;
            transactionToUpdate.Date = transaction.Date;
            transactionToUpdate.Description = transaction.Description;

            await dbContext.SaveChangesAsync();

            transactionToUpdate = await dbContext.Transactions.FirstOrDefaultAsync(t => t.Id == transaction.Id);

            if (transactionToUpdate == null)
            {
                _logger.LogInformation($"In UpdateCategoryAsync: category with id: {transaction.Id} does not exist");
                return null;
            }

            return transaction;
        }
    }
    
    /// <summary>
    /// Tries to delete a transaction from the database
    /// </summary>
    /// <param name="transaction"></param>
    /// <returns></returns>
    
    public async Task<bool> DeleteTransactionAsync(Transaction transaction)
    {
        using (var dbContext = await _dbContextProvider.GetDbContext())
        {
            var transactionToDelete = await dbContext.Transactions.FirstOrDefaultAsync(t => t.Id == transaction.Id);

            if (transactionToDelete == null)
            {
                _logger.LogInformation($"In UpdateCategoryAsync: category with id: {transaction.Id} does not exist");
                return false;
            }
            
            dbContext.Transactions.Remove(transactionToDelete);
            await dbContext.SaveChangesAsync();

            return true;
        }
    }
}