using DataAccess.Configuration;
using DataAccess.Database;
using Expense_manager.Components;

using Microsoft.EntityFrameworkCore;


var builder = WebApplication.CreateBuilder(args);
var configuration = builder.Configuration;

var connectionString = configuration.GetValue<string>("DbSettings:ConnectionString");

builder.Services.Configure<DbConfiguration>(configuration.GetSection("DbSettings"));

builder.Services.AddTransient<ITransactionRepository, TransactionRepository>();
builder.Services.AddTransient<IUserRepository, UserRepository>();
builder.Services.AddTransient<ICategoryRepository, CategoryRepository>();
builder.Services.AddSingleton<IDbContextProvider, DbContextProvider>();

builder.Services.AddDbContextFactory<ApplicationDbContext>(opt =>
{
    opt.UseNpgsql(connectionString);
});


// Add services to the container.
builder.Services.AddRazorComponents()
    .AddInteractiveServerComponents();


var app = builder.Build();
app.UseRouting();

using (var dbContext = await app.Services.GetService<IDbContextFactory<ApplicationDbContext>>()!.CreateDbContextAsync())
{
    if (dbContext.Database.GetPendingMigrations().Any())
    {
        await dbContext.Database.MigrateAsync();
    }
}


// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Error", createScopeForErrors: true);
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

app.UseHttpsRedirection();

app.UseStaticFiles();
app.UseAntiforgery();

app.MapRazorComponents<App>()
    .AddInteractiveServerRenderMode();

app.Run();